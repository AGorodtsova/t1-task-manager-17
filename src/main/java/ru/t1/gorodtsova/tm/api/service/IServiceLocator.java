package ru.t1.gorodtsova.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

}
