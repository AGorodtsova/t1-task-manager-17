package ru.t1.gorodtsova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectService().clear();
    }

}
