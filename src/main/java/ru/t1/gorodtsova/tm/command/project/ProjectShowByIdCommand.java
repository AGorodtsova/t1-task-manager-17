package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Display project by id";
    }

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

}
