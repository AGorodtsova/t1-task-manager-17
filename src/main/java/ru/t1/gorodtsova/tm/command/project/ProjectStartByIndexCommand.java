package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Start project by index";
    }

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.IN_PROGRESS);
    }

}
