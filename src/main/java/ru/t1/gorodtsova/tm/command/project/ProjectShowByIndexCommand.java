package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Display project by index";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
