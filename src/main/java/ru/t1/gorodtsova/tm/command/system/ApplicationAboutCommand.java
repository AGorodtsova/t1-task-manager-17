package ru.t1.gorodtsova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Show developer info";
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasia Gorodtsova");
        System.out.println("email: agorodtsova@t1-consulting.ru");
    }

}
