package ru.t1.gorodtsova.tm.command.system;

import ru.t1.gorodtsova.tm.api.service.ICommandService;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
